package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
   

    @Test
    public void echoReturnSame(){
        App app = new App();
        int result = app.echo(5);
        assertEquals("Testing echo method to see if it works", result,5);
    }
    @Test
    public void oneMoreExpectedResult(){
        App p=new App();
        
        assertEquals("Testing One More Method to see if it works good",6,p.oneMore(5));
    }
    
    
}
